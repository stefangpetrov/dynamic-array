#define CATCH_CONFIG_MAIN
#include "catch.h"

#include "..\dynamic-array-app\Allocator.h"
#include "..\dynamic-array-app\DynamicArray.h"

TEST_CASE("dynamic array can be initialized with different constructors", "[dynamic array]") {

    SECTION("Default constructor allocates an array with capacity 2 and size 0") {
        DynamicArray dynamicArr;

        REQUIRE(dynamicArr.size() == 0);
        REQUIRE(dynamicArr.capacity() == 2);
    }
    
    SECTION("Constructor with one property initializes an array with capacity N and size 0") {
        const size_t N = 20;
        DynamicArray dynamicArr(N);

        REQUIRE(dynamicArr.size() == 0);
        REQUIRE(dynamicArr.capacity() >= N);
    }

    SECTION("Constructor with two properties initializes an array with capacity N, size N and default value K") {
        const size_t N = 20;
        const size_t K = 0;
        DynamicArray dynamicArr(N, K);

        REQUIRE(dynamicArr.size() == N);
        REQUIRE(dynamicArr.capacity() >= N);

        for (size_t i = 0; i < N; i++)
        {
            REQUIRE(dynamicArr[i] == K);

        }
    }
    
}

TEST_CASE("dynamic array supports rule of 3", "[dynamic array]") {

    const size_t N = 20;
    const size_t K = 0;
    
    DynamicArray dynamicArr(N, K);

    REQUIRE(dynamicArr.size() == N);
    REQUIRE(dynamicArr.capacity() >= N);

    SECTION("Copy Constructor creates an array with the same properties") {
        DynamicArray dynamicArr2(dynamicArr);

        REQUIRE(dynamicArr2.size() == dynamicArr.size());
        REQUIRE(dynamicArr2.capacity() == dynamicArr.capacity());
        for (size_t i = 0; i < N; i++)
        {
            REQUIRE(dynamicArr2[i] == dynamicArr[i]);

        }
    }

    SECTION("Operator= returns an array with the same properties") {
        DynamicArray dynamicArr2;
        dynamicArr2 = dynamicArr;

        REQUIRE(dynamicArr2.size() == dynamicArr.size());
        REQUIRE(dynamicArr2.capacity() == dynamicArr.capacity());
        for (size_t i = 0; i < N; i++)
        {
            REQUIRE(dynamicArr2[i] == dynamicArr[i]);

        }
    }

    /*
    SECTION("The destructor successfully deallocates the memory of the array") {
        dynamicArr.~DynamicArray();

        REQUIRE(dynamicArr2.size() == dynamicArr.size());
        REQUIRE(dynamicArr2.capacity() == dynamicArr.capacity());
    }
    */

}

TEST_CASE("dynamic array operator[] should return the element at a given index", "[dynamic array]") {

    const size_t N = 20;
    const size_t K = 0;

    DynamicArray dynamicArr(N, K);

    for (size_t i = 0; i < dynamicArr.size(); i++)
    {
        dynamicArr[i] = i;
        REQUIRE(dynamicArr[i] == i);

    }
    dynamicArr[3] = 1000;
    REQUIRE(dynamicArr[3] == 1000);

}

TEST_CASE("dynamic array at() should return the element at a given index", "[dynamic array]") {

    const size_t N = 20;
    const size_t K = 0;

    DynamicArray dynamicArr(N, K);

    for (size_t i = 0; i < dynamicArr.size(); i++)
    {
        dynamicArr.at(i) = i;
        REQUIRE(dynamicArr.at(i) == i);

    }
    dynamicArr.at(3) = 1000;
    REQUIRE(dynamicArr.at(3) == 1000);

}

TEST_CASE("dynamic array clear should erase all elements, keep the same capacity and make the size 0", "[dynamic array]") {

    const size_t N = 10;
    const size_t K = 0;

    DynamicArray dynamicArr(N, K);

    REQUIRE(dynamicArr.size() == 10);
    REQUIRE(dynamicArr.capacity() >= 10);

    dynamicArr.clear();

    REQUIRE(dynamicArr.size() == 0);
    REQUIRE(dynamicArr.capacity() >= 10);

}

TEST_CASE("dynamic array empty should tell if there are no elements in the array", "[dynamic array]") {

    DynamicArray dynamicArr;

    REQUIRE(dynamicArr.empty() == true);

    int value = 55;
    dynamicArr.push_back(value);
    REQUIRE(dynamicArr.empty() == false);

    dynamicArr.clear();
    REQUIRE(dynamicArr.empty() == true);

}

TEST_CASE("dynamic array front method should return the element at index 0", "[dynamic array]") {

    const size_t N = 20;
    DynamicArray dynamicArr(N);

    REQUIRE_THROWS(dynamicArr.front());

    for (int i = 1; i <= N; i++)
    {
        dynamicArr.push_back(i);
        REQUIRE(dynamicArr.front() == 1);

    }

    dynamicArr[0] = 555;

    REQUIRE(dynamicArr.front() == 555);

    dynamicArr[0] = -29999;

    REQUIRE(dynamicArr.front() == -29999);

    dynamicArr.clear();
    REQUIRE_THROWS(dynamicArr.front());


}

TEST_CASE("dynamic array back method should return the element at index 0", "[dynamic array]") {

    const size_t N = 20;
    DynamicArray dynamicArr(N);

    REQUIRE_THROWS(dynamicArr.back());

    for (int i = 1; i <= N; i++)
    {
        dynamicArr.push_back(i);
        REQUIRE(dynamicArr.back() == i);
    }

    dynamicArr[N - 1] = 555;

    REQUIRE(dynamicArr.back() == 555);

    dynamicArr[N - 1] = -29999;

    REQUIRE(dynamicArr.back() == -29999);

    dynamicArr.clear();
    REQUIRE_THROWS(dynamicArr.back());
}

TEST_CASE("dynamic array push_back should insert an element at the last index in the array", "[dynamic array]") {

    DynamicArray dynamicArr;

    int value = 55;
    dynamicArr.push_back(value);
    REQUIRE(dynamicArr[dynamicArr.size() - 1] == 55);

    value = 0;
    dynamicArr.push_back(value);
    REQUIRE(dynamicArr[dynamicArr.size() - 1] == 0);

    value = -100;
    dynamicArr.push_back(value);
    REQUIRE(dynamicArr[dynamicArr.size() - 1] == -100);
}

TEST_CASE("dynamic array pop_back should remove an element from the last index in the array and decrease size with 1", "[dynamic array]") {

    DynamicArray dynamicArr;
    REQUIRE(dynamicArr.size() == 0);

    dynamicArr.pop_back();
    REQUIRE(dynamicArr.size() == 0);


    for (int i = 0; i < 5; i++)
    {
        dynamicArr.push_back(i);
    }
    for (int i = 4; i >= 0; i--)
    {
        REQUIRE(dynamicArr[dynamicArr.size() - 1] == i);
        REQUIRE(dynamicArr.size() == i + 1);
        dynamicArr.pop_back();

    }

}

TEST_CASE("dynamic array can be sized and resized", "[dynamic array]") {

    DynamicArray dynamicArr(5);

    REQUIRE(dynamicArr.size() == 0);
    REQUIRE(dynamicArr.capacity() >= 10);

    SECTION("resizing bigger changes size and capacity") {
        dynamicArr.resize(10);

        REQUIRE(dynamicArr.size() == 10);
        REQUIRE(dynamicArr.capacity() >= 10);
    }
    SECTION("resizing smaller changes size but not capacity") {
        dynamicArr.resize(0);

        REQUIRE(dynamicArr.size() == 0);
        REQUIRE(dynamicArr.capacity() >= 10);
    }
}

TEST_CASE("dynamic array can reserve memory", "[dynamic array]") {

    DynamicArray dynamicArr(5);

    REQUIRE(dynamicArr.size() == 0);
    REQUIRE(dynamicArr.capacity() == 10);

    
    SECTION("reserving bigger changes capacity but not size") {
        dynamicArr.reserve(10);

        REQUIRE(dynamicArr.size() == 0);
        REQUIRE(dynamicArr.capacity() == 10);
    }
    SECTION("reserving smaller does not change size or capacity") {
        dynamicArr.reserve(0);

        REQUIRE(dynamicArr.size() == 0);
        REQUIRE(dynamicArr.capacity() >= 5);
    }
}



