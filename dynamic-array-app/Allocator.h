#pragma once
template<class T>
class Allocator
{
public:
	T* alloc(size_t size)
	{
		return new T[size];
	}

	void dealloc(T*& ptr)
	{
		delete[] ptr;
		ptr = nullptr;
	}
};

