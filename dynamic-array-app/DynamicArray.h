#pragma once
#include "Allocator.h"

class DynamicArray
{
private:
	static const size_t f_resizeFactor = 2;
	static_assert(f_resizeFactor > 1, "Resize factor must be greater than 1");
	size_t f_capacity = 2;
	size_t f_size = 0;
	int* f_data = nullptr;
	Allocator<int> allocator;
	
private:
	int* cloneBuffer(const int* arr, size_t size, size_t elementsToCopy);
	void copy(const DynamicArray& other);

public:
	DynamicArray();
	DynamicArray(size_t _size);
	DynamicArray(size_t _size, int value);
	DynamicArray(const DynamicArray& other);
	DynamicArray& operator=(const DynamicArray& other);
	~DynamicArray();

	int& at(size_t index);
	const int& at(size_t index) const;

	int& operator[](size_t index) noexcept;
	const int& operator[](size_t index) const noexcept;



	int& front();
	const int& front() const;


	int& back();
	const int& back() const;

	void clear() noexcept;

	bool empty() const noexcept;
	
	size_t capacity() const noexcept;

	size_t size() const noexcept;



	void push_back(int& element);
	void pop_back() noexcept;

	void reserve(size_t newCap);
	void resize(size_t newSize);

	void fill(int value) noexcept;
};

