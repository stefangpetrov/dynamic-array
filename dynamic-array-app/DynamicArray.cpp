#include "DynamicArray.h"
#include <cassert>
#include <stdexcept>

int* DynamicArray::cloneBuffer(const int* arr, size_t size, size_t elementsToCopy)
{
	if (!arr || size == 0)
		throw std::invalid_argument("Cannot clone empty array");

	int* buffer = allocator.alloc(size);

	for (size_t i = 0; i < elementsToCopy; i++)
		buffer[i] = arr[i];

	return buffer;
}

void DynamicArray::copy(const DynamicArray& other)
{
	f_data = cloneBuffer(other.f_data, other.f_capacity, other.f_size);
	f_size = other.f_size;
	f_capacity = other.f_capacity;
}

DynamicArray::DynamicArray()
{
	f_data = allocator.alloc(f_capacity);
}

DynamicArray::DynamicArray(size_t _size)
{ 	
	if (_size == 0)
		throw std::invalid_argument("Array size cannot be 0");

	f_capacity = _size * f_resizeFactor;
	f_data = allocator.alloc(f_capacity);
}

DynamicArray::DynamicArray(size_t _size, int value)
	: DynamicArray(_size)
{
	f_size = _size;
	fill(value);
}

DynamicArray::DynamicArray(const DynamicArray& other)
{
	copy(other);
}

DynamicArray& DynamicArray::operator=(const DynamicArray& other)
{
	if (this != &other)
	{		
		allocator.dealloc(f_data);
		copy(other);
	}

	return *this;	
}

DynamicArray::~DynamicArray()
{
	allocator.dealloc(f_data);
}

int& DynamicArray::at(size_t index)
{
	if (index >= f_size)
	{
		throw std::out_of_range("out of range");
	}

	return f_data[index];
}

const int& DynamicArray::at(size_t index) const
{
	if (index >= f_size)
	{
		throw std::out_of_range("Index Is Out of range!");
	}

	return f_data[index];
}

int& DynamicArray::operator[](size_t index) noexcept
{
	assert(index < f_size);
	return f_data[index];
}

const int& DynamicArray::operator[](size_t index) const noexcept
{
	assert(index < f_size);

	return f_data[index];
}

int& DynamicArray::front()
{
	if (f_size == 0)
	{
		throw std::out_of_range("Calling front() on empty array");
	}
	return f_data[0];
}

const int& DynamicArray::front() const
{
	if (f_size == 0)
	{
		throw std::out_of_range("Calling front() on empty array");
	}
	return f_data[0];
}

int& DynamicArray::back()
{
	if (f_size == 0)
	{
		throw std::out_of_range("Calling back() on empty array");
	}
	return f_data[f_size - 1];
}

const int& DynamicArray::back() const
{
	if (f_size == 0)
	{
		throw std::out_of_range("Calling back() on empty array");
	}
	return f_data[f_size - 1];
}

void DynamicArray::clear() noexcept
{	
	f_size = 0;
}

bool DynamicArray::empty() const noexcept
{
	return f_size == 0;
}

size_t DynamicArray::capacity() const noexcept
{
	return f_capacity;
}

size_t DynamicArray::size() const noexcept
{
	return f_size;
}

void DynamicArray::push_back(int& element)
{
	if (f_size == f_capacity)
		reserve(f_capacity * f_resizeFactor);

	f_data[f_size] = element;
	f_size += 1;
}

void DynamicArray::pop_back() noexcept
{
	if (f_size != 0)
		--f_size;
}

void DynamicArray::reserve(size_t newCap)
{
	if (newCap > f_capacity)
	{
		int* buffer = cloneBuffer(f_data, newCap, f_size);
		allocator.dealloc(f_data);
		f_data = buffer;
		f_capacity = newCap;
	}
}

void DynamicArray::resize(size_t newSize)
{
	if (newSize > f_capacity)
	{
		reserve(newSize * f_resizeFactor);
	}

	f_size = newSize;
}

void DynamicArray::fill(int value) noexcept
{
	for (size_t i = 0; i < f_size; i++)
		f_data[i] = value;
}
